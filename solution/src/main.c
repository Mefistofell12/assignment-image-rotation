#include <stdio.h>

#include "bmp_io.h"
#include "image.h"
#include "transform.h"


static const char* const reading_messages[] = {
	[READ_OK] = "BMP изображение успешно прочтано\n",
	[READ_INVALID_SIGNATURE] = "Невалидная подпись BMP файла\n",
	[READ_INVALID_BITS] = "Не удалось прочитать пиксели изображения\n",
	[READ_INVALID_BITS_OFFSET] = "Невалидный отступ в заголовке BMP файла\n",
    [READ_INVALID_HEADER] = "Невалидный заголовок BMP файла\n",
    [READ_FILE_ERROR] = "Ошибка при чтении файла"
};


static const char* const writing_messages[] = {
	[WRITE_OK] = "Запись в BMP файл назначения прошла успешно\n",
	[WRITE_ERROR] = "Невозможно сделать запись в файл назначения\n"
};

static const char* const error_messages[] = {
    [CANT_OPEN_SRC] = "Не получилось открыть исхдное изображение\n",
    [ERROR_WHILE_CLOSING_SRC] = "Ошибка при попытке закрыть файл с исходным изображением\n",
    [CANT_OPEN_DEST] = "Не удалось открыть файл назначения\n",
    [ERROR_WHILE_CLOSING_DEST] = "Ошибка при попытке закрыть файл назначения\n",
    [WRONG_ARGS] = "Неверное количество аргументов\n"
};


static inline void print_err(const char* error_text){
    fprintf(stderr, "%s", error_text);
}


static inline void print(const char* text){
    fprintf(stdout, "%s", text);
}

int main( int argc, char** argv ) {
    if (argc != 3) {

        print_err(error_messages[WRONG_ARGS]);
        return -1;
    }


    FILE* src = fopen(argv[1], "rb");
    FILE* dest = fopen(argv[2], "wb");


    if(src == NULL)
    {
        print_err(error_messages[CANT_OPEN_SRC]);
        return -1;
    }

    if(dest == NULL)
    {
        print_err(error_messages[CANT_OPEN_DEST]);
        if(fclose(src)){

            print_err(error_messages[ERROR_WHILE_CLOSING_SRC]);
        }
        return -1;
    }

    struct image img;
    const enum read_status r_status = from_bmp(src, &img);
    if(r_status != READ_OK){

        print_err(reading_messages[r_status]);
        return -1;
    }


    struct image new_img = image_rotate(img);
    enum write_status w_status = to_bmp(dest, &new_img);

    image_free_memory(&img);
    image_free_memory(&new_img);
    if(w_status != WRITE_OK){

        print_err(writing_messages[w_status]);

        if(fclose(src)){
            print_err(error_messages[ERROR_WHILE_CLOSING_SRC]);
        }
        if(fclose(dest)){
            print_err(error_messages[ERROR_WHILE_CLOSING_DEST]);
        }
        return -1;
    }

    print(writing_messages[w_status]);


    if(fclose(src)){
        print_err(error_messages[ERROR_WHILE_CLOSING_SRC]);
    }
    if(fclose(dest)){
        print_err(error_messages[ERROR_WHILE_CLOSING_DEST]);
    }
    return 0;
}



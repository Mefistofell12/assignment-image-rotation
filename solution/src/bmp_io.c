#include <stdint.h>

#include "bmp_io.h"
#include "image.h"

#define COMPRESSION_BI_RGB 0
#define DIB_HEADER_SIZE 40
#define BITS_PER_PIXEL 24
#define BMP_FILE_SIGNATURE 0x004D42
#define PLANES_NUM 1

struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed));


static enum header_read_status read_bmp_header(FILE* file, struct bmp_header* header){
    const size_t numblocks_count = fread(header, (sizeof( struct bmp_header )), 1, file);

    if(numblocks_count == 1){
        return READ_HEADER_OK;
    }
    return READ_HEADER_ERROR;
}


static inline uint32_t get_padding_size(const uint32_t width){
    return width % 4;
}


static enum pixels_read_status read_pixels(FILE* file, struct image* img) {
    struct pixel* pixels = img->data;
	struct pixel* ptr = pixels;
	const uint32_t padding = get_padding_size(img->width);

	for(uint32_t i = 0; i < img->height; i++) {
		const size_t px_count = fread(ptr, sizeof(struct pixel), img->width, file);
		if(px_count != img->width){
			free(pixels);
			return READ_PIXELS_ERROR;
		}
		const int fseek_success = fseek(file, padding, SEEK_CUR);
		if(fseek_success != 0){
			free(pixels);
			return READ_PIXELS_ERROR;
		}
		ptr = ptr + img->width;
	}
	return READ_PIXELS_OK;
}


static enum write_status write_pixels(FILE* file, struct pixel* pixels, const uint32_t width, const uint32_t height, const uint32_t padding_bytes) {
	struct pixel* ptr = pixels;
    for(int i = 0; i < height; i++){
		const size_t cnt = fwrite(ptr, sizeof(struct pixel), width, file);
		if(cnt != width){
			return WRITE_ERROR;
		}
		const size_t pad = fwrite((void*) ptr, 1, padding_bytes, file);
		if(pad != padding_bytes){
			return WRITE_ERROR;
		}
		ptr = ptr + width;
    }
	return WRITE_OK;
}


static enum read_status validate_bmp_header(struct bmp_header const * const header) {
	if (header->bfType != BMP_FILE_SIGNATURE) {
		return READ_INVALID_SIGNATURE;
	}
	if (header->biBitCount != BITS_PER_PIXEL) {
		return READ_INVALID_BITS;
	}
	if (header->bOffBits != (sizeof( struct bmp_header ))) {
		return READ_INVALID_HEADER;
	}
	return READ_OK;
}


static struct bmp_header create_simple_bmp_header(const uint32_t width, const uint32_t height, const uint32_t padding){
    const uint32_t imgage_size = height*padding + width*height*sizeof(struct pixel);
    return (struct bmp_header)
    {
        .bfType = BMP_FILE_SIGNATURE,
        .bfileSize = (sizeof( struct bmp_header )) + imgage_size,
        .bfReserved = 0,
        .bOffBits = (sizeof( struct bmp_header )),
        .biSize = DIB_HEADER_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES_NUM,
        .biBitCount = BITS_PER_PIXEL,
        .biCompression = COMPRESSION_BI_RGB,
        .biSizeImage = imgage_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0,
    };     
}


enum read_status from_bmp(FILE* file, struct image* img){
	struct bmp_header header = (struct bmp_header) {0};
	const enum header_read_status header_status = read_bmp_header(file, &header);
	if(header_status == READ_HEADER_ERROR){
		return READ_FILE_ERROR;
	}

	const enum read_status validation_result = validate_bmp_header(&header);
	if (validation_result != READ_OK){
        return validation_result;
    }
	
	*img = image_create(header.biWidth, header.biHeight);
	const int any_error = fseek(file, header.bOffBits, SEEK_SET);
	if(any_error){
		return READ_INVALID_BITS_OFFSET;
	}
	
	const enum pixels_read_status px_read = read_pixels(file, img);
	if (px_read == READ_PIXELS_ERROR) {
		return READ_INVALID_BITS;
	}
	return READ_OK;
}


enum write_status to_bmp(FILE* file, struct image const* img){
    const uint32_t width = img->width;
	const uint32_t height = img->height;
	const uint32_t padding = get_padding_size(width);
	struct pixel* px_array = img->data;
	struct bmp_header header = create_simple_bmp_header(width, height, padding);
	
	if (fwrite(&header, sizeof(struct bmp_header), 1, file) != 1){
        return WRITE_ERROR;
    }	
	return write_pixels(file, px_array, width, height, padding);
}


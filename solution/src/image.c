#include <malloc.h>
#include <stdio.h>

#include "image.h"


struct image image_create(const uint64_t width, const uint64_t height){
	struct pixel* pixels = malloc(sizeof(struct pixel) * (width * height));
	return (struct image) { .width = width, .height = height, .data = pixels };
}


inline void image_free_memory(struct image* image) {
	free(image->data);
}


#ifndef IMAGE_H
#define IMAGE_H

#include <inttypes.h>


struct pixel {
    uint8_t b, g, r;
} __attribute__((packed));

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(uint64_t width, uint64_t height);
void image_free_memory(struct image* image);

#endif


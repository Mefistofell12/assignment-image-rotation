#ifndef BMP_IO_H
#define BMP_IO_H

#include "image.h"
#include <stdint.h>
#include <malloc.h>
#include <stdio.h>


enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum header_read_status {
    READ_HEADER_OK = 0,
    READ_HEADER_ERROR
};

enum pixels_read_status {
    READ_PIXELS_OK = 0,
    READ_PIXELS_ERROR
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_BITS_OFFSET,
    READ_INVALID_HEADER,
    READ_FILE_ERROR
};

enum common_errors {
    CANT_OPEN_SRC = 0,
    ERROR_WHILE_CLOSING_SRC,
    CANT_OPEN_DEST,
    ERROR_WHILE_CLOSING_DEST,
    WRONG_ARGS
};


enum read_status from_bmp(FILE* file, struct image* img);


enum write_status to_bmp(FILE* file, struct image const* img);

#endif


